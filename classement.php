<?php
session_start();
?>
<!DOCTYPE html>

<html>
<head>
	<meta charset = "UTF_8"></meta>
		<link rel="stylesheet" href="style.css" />
	
</head>
<body>

		<table border =1>
		<h1> Classement des oeuvres : </h1>
		<?php
			include ('fonction.php');
			$bdd = connection();
			
			try{
			$sql = $bdd->prepare("select titre, artiste, avg(note) as note from musiqueed.oeuvre join musiqueed.note on oeuvre.idOeuvre = note.idOeuvre group by note.idOeuvre order by note desc");
			$sql->execute();
			
			echo "<tr>
					<th> Titre </th>
					<th> Artiste </th>
					<th> Note moyenne </th>
				</tr>";
			
			
			while($resultat = $sql->fetch(PDO::FETCH_OBJ)){
					echo "
							<td> " .$resultat->titre ." </td>
							<td> " .$resultat->artiste . " </td>
							<td> " .$resultat->note . " </td>

							
						<tr>
					";
				}
			
			}
			
			catch(PDOException $e){
				echo "Erreur dans la requête" .$e->getMessage();
			}
			?>
			</table>

		</br>
		</br>
		<a href ="index.html" value ="retour" class="btn btn-primary"><span class ="user">  Retour à l'accueil </a>


</body>
</html>